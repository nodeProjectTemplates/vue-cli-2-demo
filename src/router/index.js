import Vue from 'vue'
import Router from 'vue-router'
import DemoIndex from '@/components/demo/DemoIndex.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'DemoIndex',
      component: DemoIndex
    }
  ]
})
