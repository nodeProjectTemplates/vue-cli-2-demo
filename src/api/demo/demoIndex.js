import request from '@/utils/request'

export function getData (id) {
  return request({
    url: `/api/server1/getData/${id}`, // 通过 /config/index.js 中 proxyTable 代理转发请求
    method: 'GET'
  })
}
