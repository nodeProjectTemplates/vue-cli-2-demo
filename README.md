# vue-cli-2-demo

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

```
├─build                         // webpack开发打包相关配置
│ ├─build.js                    // 构建环境下的配置:loading动画;删除创建目标文件夹;webpack编译;输出信息
│ ├─check-versions.js           // node和npm的版本检查
│ ├─logo.png
│ ├─utils.js                    // 配置静态资源路径；cssLoaders用于加载.vue文件中的样式；styleLoaders用于加载不在.vue文件中的单独存在的样式文件
│ ├─vue-loader.conf.js
│ ├─webpack-base.conf.js        // 基本的webpack配置.配置webpack编译入口/webpack输出路径和命名规则/模块resolve规则/不同类型模块的处理规则
│ ├─webpack-dev.conf.js         // 开发环境配置,在base.conf基础进一步完善;合并基础webpack配置;将hot-reload相关的代码添加到entry chunks;使用styleLoaders;配置Source Maps / webpack插件;
│ ├─webpack-prod.conf.js        // 生产环境配置,在base.conf基础进一步完善;合并基础webpack配置;使用styleLoaders;配置webpack输出/webpack插件;gzip模式下的webpack插件配置;webpack-bundle分析
├─config                        // 项目配置（端口号等）
│ ├─dev.env.js
│ ├─index.js                    // 用于定义开发环境和生产环境所需要的参数
│ ├─prod.env.js
├─dist                          // cnpm run build  项目打包后生成的文件夹
├─node_modules                  // cnpm install    项目依赖模块
├─src
│ ├─assets                      // 资源目录（放置一些图片等），这里的资源会被webpack构建
│ ├─components                  // 组件
│ ├─router                      // 路由
│ ├─App.vue                     // 根组件
│ ├─main.js                     // 入口js
├─static                        // 纯静态资源（不会变动的资源，如图片、字体），不会被webpack构建，直接被复制到打包目录dist/static
├─.babelrc                      // 使用babel的配置文件，用来设置转码规则和插件
├─.editorconfig                 // 代码的规范文件（规定使用空格或tab缩进，缩进的长度等，使用的话需要在编辑器下载对应的插件）
├─.gitignore                    // 指定 git 忽略的文件，所有 git 操作均不会对其生效
├─.postcssrc.js                 // 指定使用的 css 预编译器，里面默认配置了 autoprefixer ，自动补全浏览器前缀
├─index.html                    // 入口页面
├─package-lock.json             // 确定当前安装的包的依赖，以便后续重新安装的时候生成相同的依赖，而忽略项目开发过程中有些依赖已经发生的更新
├─package.json                  // 项目配置文件
└─README.md                     // 备注文件，对项目开发过程中需要注意的地方进行一些说明
```
